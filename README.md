# MECH5765M Examples

There are two simscape examples in this folder.

### Example 1 
Can be found inside ./SimpleUAV and consists of a basic model of the Penguin BE.

Assumptions and simplifications of the model include:
  * No moments considered about any axis
  * Constant lift and drag coefficients
  * No lateral forces considered

The model illustrates how simulink and simscape can be used together without any additional libraries to model an aircraft. This works well for simplified models but a more robust approach (like using the aeropace blockset) should be used if high fidelity is required.

Suggested activities:
  * Open UAVFlightSim.slx and explore the model. To run it, run the script RunSimpleFlightSim.m and play around with the launch force and launch direction.
  * Open UAVAltitudeControl.slx and explore the PID control strategy. To run the model, run the script: RunSpeedControlSim.m. Play around with the PID gains and see if you can make the system more responsive or unstable. Modify the desired speed curve by editing the signal generator block.

### Example 2 
Can be found inside ./AileronActuationSimple and consists of a more complex model of an aileron and it's actuation system. The aileron geometry and properties are imported directly from CAD files using simscape import tools.

There are two versions of the simulation: InverseDynamicsSim.slx and HydraulicSim.slx. To run them both, execute the script: RunExample.m.
  * InverseDynamicsSim.slx uses an idealized actuator that has no dynamic limitations. This can be used to understand the requirements of the system in termsn of actuation force needed.
  * HydraulicSim.slx uses three hydraulic actuators connected to a single reservoir. These actuators have real physical limitations defined in RunExample.m. They will not follow the command curve as closely as the idealized actuators, but give a more accurate prediction of expected real-life performance. 

Suggeted activities:
  * Open InverseDynamicsSim.slx, explore the model and try to understand how the actuation is idealized. 
  * Execute RunExample.m and think about how you would use the idealized simulation to size the hydraulic pistons used in HydraulicSim.slx. 
  * Play with the hydraulic piston properties in RunExample.m to see if you can improve the performance of the system. Think about how you could use this to make informed decissisions on how to design the hydraulic system. 
  * Try to modify the mass of the aileron by modifying its density and see how this affects the performance of the system. 
