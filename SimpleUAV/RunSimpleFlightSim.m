% Clear console and workspace
clc
clear

% Define variables related to UAV initialization
xCoord = 15; %[m]
yCoord = 1.5; %[m]
zCoord = 1.3; %[m]
launchForce = 3000; %[N]
zRot = deg2rad(180);
c = cos(zRot);
s = sin(zRot);
rotMat = [c -s 0; s c 0; 0 0 1];
launchDelay = 0.2; %[s]
launchDuration = 0.3; %[s]

% Add current path to matlabpath to find STLs with relative locations
addpath(pwd);

% Run the sim and collect outputs to simout
simout = sim('UAVFlightSim.slx');

% Place outputs into more accessible elements
simTime = simout.tout;
UAVPos = simout.yout{1}.Values.Data;
UAVVel = simout.yout{2}.Values.Data;
UAVAcc = simout.yout{3}.Values.Data;

% Plot the trajectory
for i = 1:3
    % Position
    figure(1)
    hold on
    subplot(3, 1, i)
    plot(simTime, UAVPos(:, i))
    xlabel("Time (s)")
    ylabel("Position (m)")
    grid on
    
    % Velocity
    figure(2)
    hold on
    subplot(3, 1, i)
    plot(simTime, UAVVel(:, i))
    xlabel("Time (s)")
    ylabel("Velocity (m/s)")
    grid on
end

    