% Clear console and workspace
clc
clear

% Define variables related to UAV initialization
xCoord = 15; %[m]
yCoord = 1.5; %[m]
zCoord = 1.3; %[m]
launchForce = 3000; %[N]
zRot = deg2rad(180);
c = cos(zRot);
s = sin(zRot);
rotMat = [c -s 0; s c 0; 0 0 1];
launchDelay = 0.1; %[s]
launchDuration = 0.3; %[s]

% Add current path to matlabpath to find STLs with relative locations
addpath(pwd);

% Run the sim and collect outputs to simout
simout = sim('UAVSpeedControl.slx');

% Place outputs into more accessible elements
simTime = simout.tout;
desiredSpeed = simout.yout{1}.Values.Data;
actualSpeed = simout.yout{2}.Values.Data;
commandedThrottle = simout.yout{3}.Values.Data;

% Plot the controller response
figure(1)
subplot(2, 1, 1)
hold on
plot(simTime, desiredSpeed)
plot(simTime, actualSpeed)
xlabel("Time (s)")
ylabel("Velocity (m/s)")
legend("Desired", "Achieved")
grid on
hold off

subplot(2, 1, 2)
plot(simTime, commandedThrottle*100)
xlabel("Time (s)")
ylabel("Throttle command (%)")
grid on



    