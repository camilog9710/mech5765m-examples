There are several other ways to import assemblies into Simscape. 
For more information, refer to: 

https://uk.mathworks.com/help/physmod/smlink/ref/linking-and-unlinking-simmechanics-link-software-with-solidworks.html
https://uk.mathworks.com/help/physmod/sm/ug/import-a-cad-assembly-model.html

The best option for you depends on what you want to achieve and what you already have...