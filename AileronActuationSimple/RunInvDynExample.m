clear
clc

% Import the CAD model
aileronAssembly_DataFile

% Add necessary folders to path
addpath(pwd + "\CAD")

% Define command angle profile
qProfile = [0     0
            0.9   0
            1.4   10.0
            2.0   10.0
            2.3   17.5
            3.0   17.5
            4.5  -15.0
            5.0  -15.0
            5.5  -20.0
            6.5  -20.0
            8.0   15.0
            8.5   15.0
            9.5   0
           10.0   0];

% Variables for ideal system
AAC_Param.Act.Id.Spr.k  = 10000; % Stiffness of prismatic joint [N/m]
AAC_Param.Act.Id.Spr.l0 = -0.076/3; % Equilibrium position of the prismatic joint [m]
AAC_Param.Act.Id.Spr.b  = 5e4/3; % Damping coefficient of prismatic joint [N/(m/s)]
ang2Ext_coeff = [3.77549397585510e-08,-3.77026163170581e-06,-0.00111323892045912,0.0349729536631344]; %Map to convert from input angle to input extension

% Run the inverse dynamics sim
IDSim = sim('InverseDynamicsSim.slx');
IDTime = IDSim.tout;
IDq = IDSim.yout{1}.Values.Data;
IDfz = IDSim.yout{2}.Values.Data;

% Plot and compare results
figure(1)
clf;
subplot(2, 1, 1)
hold on
plot(qProfile(:, 1), qProfile(:, 2), "k", "LineWidth", 2)
plot(IDTime, IDq, "r", "LineWidth", 1.5)
hold off
legend("Desired", "Ideal actuators")
xlabel("Time (s)")
ylabel("Aileron incidence (deg)")
grid on

subplot(2, 1, 2)
hold on
plot(IDTime, IDfz, "r", "LineWidth", 1.5)
hold off
xlabel("Time (s)")
ylabel("Push force (N)")
grid on

