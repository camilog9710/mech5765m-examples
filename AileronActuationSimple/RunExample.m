clear
clc

% Import the CAD model
aileronAssembly_DataFile

% Add necessary folders to path
addpath(pwd + "\CAD")

% Define command angle profile
qProfile = [0     0
            0.9   0
            1.4   10.0
            2.0   10.0
            2.3   17.5
            3.0   17.5
            4.5  -15.0
            5.0  -15.0
            5.5  -20.0
            6.5  -20.0
            8.0   15.0
            8.5   15.0
            9.5   0
           10.0   0];

% Variables for ideal system
AAC_Param.Act.Id.Spr.k  = 10000; % Stiffness of prismatic joint [N/m]
AAC_Param.Act.Id.Spr.l0 = -0.076/3; % Equilibrium position of the prismatic joint [m]
AAC_Param.Act.Id.Spr.b  = 5e4/3; % Damping coefficient of prismatic joint [N/(m/s)]
ang2Ext_coeff = [3.77549397585510e-08,-3.77026163170581e-06,-0.00111323892045912,0.0349729536631344]; %Map to convert from input angle to input extension

% Variables for hydraulic system
AAC_Param.Act.Hy.Spr.k  = 10000; % Stiffness of prismatic joint [N/m]
AAC_Param.Act.Hy.Spr.l0 = -0.076/3; % Equilibrium position of the prismatic joint [m]
AAC_Param.Act.Hy.Spr.b  = 5e4/3; % Damping coefficient of prismatic joint [N/(m/s)]

% Valve characterstics
AAC_Param.Act.Hy.vl.maxo    = 0.005; % Maximum opening distance [m]
AAC_Param.Act.Hy.vl.maxa    = 1e-5; % Maximum opening area [m^2]

% Initial opening of the P-A, P-B, A-T, B-T channels. 
% Negative value means overlapped valve see link below for more info:
% https://www.hawe.com/fluid-lexicon/overlap-in-valves/
AAC_Param.Act.Hy.vl.pax0    = -1e-6;
AAC_Param.Act.Hy.vl.pbx0    = -1e-6;
AAC_Param.Act.Hy.vl.atx0    = -1e-6;
AAC_Param.Act.Hy.vl.btx0    = -1e-6;

% Initial conditions for piston
AAC_Param.Act.Hy.pa0        = 1e6;      % Pressure on side A [Pa]
AAC_Param.Act.Hy.pb0        = 1e6;      % Pressure on side B [Pa]
AAC_Param.Act.Hy.x0         = 0.035028; % Starting position from A [m]

% Piston characteristics
AAC_Param.Act.Hy.deadvol    = 1e-6; % Dead volume [m^3]
AAC_Param.Piston.d      = 0.019; % Diameter [m] 
AAC_Param.Piston.area   = (AAC_Param.Piston.d/2)^2*pi; % Cross sectional area [m^2]
AAC_Param.Piston.stroke = 0.06; % Stroke [m]

Kp = 0.3; % Proportional gain
Ki = 0.3; % Integral gain
hydr_supply_pressure        = 2.0e6; % Pa

% Run the inverse dynamics sim
IDSim = sim('InverseDynamicsSim.slx');
IDTime = IDSim.tout;
IDq = IDSim.yout{1}.Values.Data;
IDfz = IDSim.yout{2}.Values.Data;

% Run the hydraulic sim
HySim = sim('HydraulicSim.slx');
HyTime = HySim.tout;
Hyq = HySim.yout{1}.Values.Data;
Hyfz = HySim.yout{2}.Values.Data;

% Plot and compare results
figure(1)
clf;
subplot(2, 1, 1)
hold on
plot(qProfile(:, 1), qProfile(:, 2), "k", "LineWidth", 2)
plot(IDTime, IDq, "r", "LineWidth", 1.5)
plot(HyTime, Hyq, "b", "LineWidth", 1.5)
hold off
legend("Desired", "Ideal actuators", "Hydraulic actuators")
xlabel("Time (s)")
ylabel("Aileron incidence (deg)")
grid on

subplot(2, 1, 2)
hold on
plot(IDTime, IDfz, "r", "LineWidth", 1.5)
plot(HyTime, Hyfz, "b", "LineWidth", 1.5)
hold off
legend("Ideal actuators", "Hydraulic actuators")
xlabel("Time (s)")
ylabel("Push force (N)")
grid on

